package com.gitee.easyopen.server.api.param;

import com.gitee.easyopen.doc.annotation.ApiDocField;
import com.gitee.easyopen.server.api.result.Goods;

/**
 * @author tanghc
 */
public class Goods2 {
    @ApiDocField(description = "goods1")
    private Goods goods;
    @ApiDocField(description = "goods2")
    private Goods goods2;
    @ApiDocField(description = "goods3")
    private Goods goods3;
    @ApiDocField(description = "goods4")
    private Goods goods4;

    public Goods getGoods() {
        return goods;
    }

    public void setGoods(Goods goods) {
        this.goods = goods;
    }

    public Goods getGoods2() {
        return goods2;
    }

    public void setGoods2(Goods goods2) {
        this.goods2 = goods2;
    }

    public Goods getGoods3() {
        return goods3;
    }

    public void setGoods3(Goods goods3) {
        this.goods3 = goods3;
    }

    public Goods getGoods4() {
        return goods4;
    }

    public void setGoods4(Goods goods4) {
        this.goods4 = goods4;
    }
}
