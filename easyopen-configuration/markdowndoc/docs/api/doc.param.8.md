
# 参数方式8，重复参数

---


<table>
    <tr>
        <th>接口名</th>
        <td>doc.param.8</td>
        <th>版本号</th>
        <td></td>
    </tr>
</table>

**请求参数**

<table>
    <tr>
        <th>名称</th>
        <th>类型</th>
        <th>是否必须</th>
        <th>最大长度</th>
        <th>示例值</th>
        <th>描述</th>
    </tr>
        <tr><td>goods</td><td class="param-type">object</td><td>否</td><td></td><td><table parentname="goods"><tr><th>名称</th><th>类型</th><th>是否必须</th><th>最大长度</th><th>示例值</th><th>描述</th></tr><tr><td>id</td><td class="param-type">long</td><td>否</td><td></td><td></td><td>id<br/></td></tr><tr><td>goods_name</td><td class="param-type">string</td><td>否</td><td>64</td><td></td><td>商品名称<br/></td></tr><tr><td>price</td><td class="param-type">float</td><td>否</td><td></td><td></td><td>价格<br/></td></tr></table></td><td>goods1<br/></td></tr>
        <tr><td>goods2</td><td class="param-type">object</td><td>否</td><td></td><td><table parentname="goods2"><tr><th>名称</th><th>类型</th><th>是否必须</th><th>最大长度</th><th>示例值</th><th>描述</th></tr><tr><td>id</td><td class="param-type">long</td><td>否</td><td></td><td></td><td>id<br/></td></tr><tr><td>goods_name</td><td class="param-type">string</td><td>否</td><td>64</td><td></td><td>商品名称<br/></td></tr><tr><td>price</td><td class="param-type">float</td><td>否</td><td></td><td></td><td>价格<br/></td></tr></table></td><td>goods2<br/></td></tr>
        <tr><td>goods3</td><td class="param-type">object</td><td>否</td><td></td><td><table parentname="goods3"><tr><th>名称</th><th>类型</th><th>是否必须</th><th>最大长度</th><th>示例值</th><th>描述</th></tr><tr><td>id</td><td class="param-type">long</td><td>否</td><td></td><td></td><td>id<br/></td></tr><tr><td>goods_name</td><td class="param-type">string</td><td>否</td><td>64</td><td></td><td>商品名称<br/></td></tr><tr><td>price</td><td class="param-type">float</td><td>否</td><td></td><td></td><td>价格<br/></td></tr></table></td><td>goods3<br/></td></tr>
        <tr><td>goods4</td><td class="param-type">object</td><td>否</td><td></td><td><table parentname="goods4"><tr><th>名称</th><th>类型</th><th>是否必须</th><th>最大长度</th><th>示例值</th><th>描述</th></tr><tr><td>id</td><td class="param-type">long</td><td>否</td><td></td><td></td><td>id<br/></td></tr><tr><td>goods_name</td><td class="param-type">string</td><td>否</td><td>64</td><td></td><td>商品名称<br/></td></tr><tr><td>price</td><td class="param-type">float</td><td>否</td><td></td><td></td><td>价格<br/></td></tr></table></td><td>goods4<br/></td></tr>
    </table>

**参数示例**

```json
{
	"goods":{
		"goods_name":"",
		"price":"",
		"id":""
	},
	"goods4":{
		"goods_name":"",
		"price":"",
		"id":""
	},
	"goods3":{
		"goods_name":"",
		"price":"",
		"id":""
	},
	"goods2":{
		"goods_name":"",
		"price":"",
		"id":""
	}
}
```

**返回结果**

<table>
    <tr>
        <th>名称</th>
        <th>类型</th>
        <th>描述</th>
    </tr>
    <tr>
        <td>code</td>
        <td>string</td>
        <td>状态值，"0"表示成功，其它都是失败</td>
    </tr>
    <tr>
        <td>msg</td>
        <td>string</td>
        <td>错误信息，出错时显示</td>
    </tr>
        <tr>
        <td>data</td>
        <td>object</td>
        <td>返回的数据，没有则返回{}
            <table>
                <tr>
                    <th>名称</th>
                    <th>类型</th>
                    <th>最大长度</th>
                    <th>示例值</th>
                    <th>描述</th>
                </tr>
                            </table>
        </td>
    </tr>
    </table>

**返回示例**

```json
{
	"code":"0",
	"data":{},
	"msg":""
}
```


