
# 返回结果10，多个list同样元素

---


<table>
    <tr>
        <th>接口名</th>
        <td>doc.result.10</td>
        <th>版本号</th>
        <td></td>
    </tr>
</table>

**请求参数**

<table>
    <tr>
        <th>名称</th>
        <th>类型</th>
        <th>是否必须</th>
        <th>最大长度</th>
        <th>示例值</th>
        <th>描述</th>
    </tr>
    </table>

**参数示例**

```json
{}
```

**返回结果**

<table>
    <tr>
        <th>名称</th>
        <th>类型</th>
        <th>描述</th>
    </tr>
    <tr>
        <td>code</td>
        <td>string</td>
        <td>状态值，"0"表示成功，其它都是失败</td>
    </tr>
    <tr>
        <td>msg</td>
        <td>string</td>
        <td>错误信息，出错时显示</td>
    </tr>
        <tr>
        <td>data</td>
        <td>object</td>
        <td>返回的数据，没有则返回{}
            <table>
                <tr>
                    <th>名称</th>
                    <th>类型</th>
                    <th>最大长度</th>
                    <th>示例值</th>
                    <th>描述</th>
                </tr>
                                <tr><td>list1</td><td>array</td><td></td><td><table><tr><th>名称</th><th>类型</th><th>最大长度</th><th>示例值</th><th>描述</th></tr><tr><td>id</td><td>long</td><td></td><td></td><td>id<br/></td></tr><tr><td>goods_name</td><td>string</td><td>64</td><td></td><td>商品名称<br/></td></tr><tr><td>price</td><td>float</td><td></td><td></td><td>价格<br/></td></tr></table></td><td>list1<br/></td></tr>
                                <tr><td>list2</td><td>array</td><td></td><td><table><tr><th>名称</th><th>类型</th><th>最大长度</th><th>示例值</th><th>描述</th></tr><tr><td>id</td><td>long</td><td></td><td></td><td>id<br/></td></tr><tr><td>goods_name</td><td>string</td><td>64</td><td></td><td>商品名称<br/></td></tr><tr><td>price</td><td>float</td><td></td><td></td><td>价格<br/></td></tr></table></td><td>list2<br/></td></tr>
                                <tr><td>list3</td><td>array</td><td></td><td><table><tr><th>名称</th><th>类型</th><th>最大长度</th><th>示例值</th><th>描述</th></tr><tr><td>id</td><td>long</td><td></td><td></td><td>id<br/></td></tr><tr><td>goods_name</td><td>string</td><td>64</td><td></td><td>商品名称<br/></td></tr><tr><td>price</td><td>float</td><td></td><td></td><td>价格<br/></td></tr></table></td><td>list3<br/></td></tr>
                                <tr><td>list4</td><td>array</td><td></td><td><table><tr><th>名称</th><th>类型</th><th>最大长度</th><th>示例值</th><th>描述</th></tr><tr><td>id</td><td>long</td><td></td><td></td><td>id<br/></td></tr><tr><td>goods_name</td><td>string</td><td>64</td><td></td><td>商品名称<br/></td></tr><tr><td>price</td><td>float</td><td></td><td></td><td>价格<br/></td></tr></table></td><td>list4<br/></td></tr>
                            </table>
        </td>
    </tr>
    </table>

**返回示例**

```json
{
	"code":"0",
	"data":{
		"list1":[
			{
				"goods_name":"",
				"price":"",
				"id":""
			}
		],
		"list4":[
			{
				"goods_name":"",
				"price":"",
				"id":""
			}
		],
		"list3":[
			{
				"goods_name":"",
				"price":"",
				"id":""
			}
		],
		"list2":[
			{
				"goods_name":"",
				"price":"",
				"id":""
			}
		]
	},
	"msg":""
}
```


